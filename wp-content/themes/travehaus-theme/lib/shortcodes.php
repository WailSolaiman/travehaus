<?php

function shortcodeFunction_get_post( $atts ) {
  global $post;
  $return = '';

  $gallery_ids = rwmb_meta( 'slider_gallery', $args = '', $post_id = $post->ID );
  $image_counter = 1;

  $return  .= '<div class="row">
  <div class="col-md-12">
  <div class="row">';

  if ( $gallery_ids ) {
    foreach ( $gallery_ids as $gallery_id ) {
      $image = $gallery_id;

      if (isset($image['post_title'])) {
        $image_title = $image['post_title'];
      }
      if (isset($image['caption'])) {
        $image_caption = $image['caption'];
      }

      $thumbimg = wp_get_attachment_url( $gallery_id['ID'], $size = 'full', false );

      if ($image_counter <= 3 && $image_counter <= 5 ) {
        $return .= '<div class="col-md-4"><img src="' . $thumbimg . '" alt="" class="img-responsive" /><span>'.$image_caption.'</span></div>';
      } elseif ($image_counter >= 4 && $image_counter <= 5) {
        $return .= '<div class="col-md-6"><img src="' . $thumbimg . '" alt="" class="img-responsive" /><span>'.$image_caption.'</span></div>';
      } elseif ($image_counter >= 6) {
        $return .= '';
      }

      if ($image_counter % 3 === 0) {
        $return  .= '</div>
        <div class="row">';
      }
      $image_counter++;
    }
  }

  return $return;
  wp_reset_postdata();

}
add_shortcode( 'get_home_images', 'shortcodeFunction_get_post' );

function shortcodeFunction_get_pricetable( $atts ) {

  extract( shortcode_atts( array(
    'saison' => 'Hauptsaison',
    'date1' => '15. Mai bis 14. Sept.',
    'date2' => '15. Mai bis 14. Sept.',
    'price1' => '130 Euro',
    'price2' => '140 Euro',
  ), $atts ) );

  $return = '';

  $return  .= '    <div class="col-md-4 price">
  <div class="content-wrapper">
  <h2>'.$saison.'</h2>
  <p class="date">'.$date1.'</p>
  <p class="date">'.$date2.'</p>
  <hr>
  <p class="personen">1 bis 2 Personen</p>
  <h3 class="single-price">'.$price1.'</h3>
  <hr>
  <p class="personen">3 bis 4 Personen</p>
  <h3 class="single-price">'.$price2.'</h3>
  <p class="last">pro Übernachtung</p>
  </div>
  </div>';

  return $return;
  wp_reset_postdata();
}

add_shortcode( 'get_pricetable', 'shortcodeFunction_get_pricetable' );

?>
