<footer class="content-info">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3>Kontakt</h3>
          <?php echo do_shortcode( '[contact-form-7 id="10" title="Footer Contact"]' ); ?>
      </div>
      <div class="col-md-3">
        <?php dynamic_sidebar('sidebar-footer'); ?>
      </div>
      <div class="col-md-3">
        <a href="/impressum" class="footer-link">Impressum</a>
      </div>
    </div>
  </div>
</footer>
