<?php use Roots\Sage\Titles; ?>



<div class="sep-60"></div>

<div class="page-header row">
  <div class="col-md-2"></div>
  <h1 class="col-md-8"><?= Titles\title(); ?></h1>
  <div class="col-md-2"></div>
</div>
